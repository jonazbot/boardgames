package boardgames;

import boardgames.GUI.GUIController;
import com.jpro.webapi.JProApplication;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JPro - JavaFX application
 *
 * Sets up the basic boardgames.GUI frame and Menu bar and a boardgames.game pane for games.
 */
public class GameApp extends JProApplication {

    public static void launch(String[] args) {
        JProApplication.launch(args);
    }

    @Override
    public void start(Stage stage) {

        // Setup boardgames.GUI stage and scene with menu and boardgames.game pane
        FXMLLoader guiLoader = new FXMLLoader(getClass().getResource("/GUI.fxml"));
        GUIController controller = new GUIController();
        guiLoader.setController(controller);

        try { guiLoader.load(); }
        catch (IOException e) { e.printStackTrace(); }

        // Sets stylesheet
        Scene appFrame = guiLoader.getRoot();
        appFrame.getStylesheets().add(getClass().getResource("/GUI.css").toExternalForm());

        // Sets up and shows Stage
        stage.setTitle("INF101 Assignment2");
        stage.setScene(appFrame);
        stage.sizeToScene();
        stage.show();

    }


}