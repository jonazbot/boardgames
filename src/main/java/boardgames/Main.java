package boardgames;

/**
 * BoardGames
 *
 * @Author Jonas Valen
 * @Email jonasvalen@gmail.com
 *
 *
 */


public class Main {

    public static void main(String[] args) {
        GameApp.launch(args);
    }

}