package boardgames.AI;

import boardgames.game.IGame;

public interface IAI {
    void makeMove(IGame game);
}
