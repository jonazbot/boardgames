package boardgames.AI;

import boardgames.game.IGame;
import boardgames.grid.Coordinate;
import java.util.List;
import java.util.Random;

/**
 * Child like AI
 * Gets a list of available moves and picks one randomly.
 *
 * Believes in a flat earth without gravity. Still complains after falling.
 *
 */

public class GameAIChild implements IAI{

    @Override
    public void makeMove(IGame game) {

        Random generateMove = new Random();
        List moves = game.getGrid().getAvailable();
        Coordinate move = (Coordinate) moves.get(generateMove.nextInt(moves.size()));
        game.checkValidMove(move.getX(), move.getY());
    }


}
