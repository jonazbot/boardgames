package boardgames.objects;

import java.util.Arrays;
import java.util.List;

/**
 * Player tag used to mark Player tiles Grid
 */
public enum PlayerID {
    P1, P2;

    /**
     * List of PlayerID tags
     */
    public static final List<PlayerID> PLAYERS = Arrays.asList(P1, P2);

}
