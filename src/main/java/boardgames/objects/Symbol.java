package boardgames.objects;

/**
 * Symbols used in TicTacToeGame
 */
public enum Symbol { X, O}

