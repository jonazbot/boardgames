package boardgames.objects;

import javafx.scene.paint.Color;

public interface IPlayer {


    /**
     * @param on Toggles AI on for this player
     */
    void toggleAI(Boolean on);

    /**
     * @return True if AI is toggled, false otherwise
     */
    boolean isAIToggled();

    /**
     * @param name Sets the name of the Player
     */
    void setPlayerName(String name);

    /**
     * @return Gets the name of the Player
     */
    String getPlayerName();

    /**
     * @return String, of the number of won games
     */
    String getVictories();

    /**
     * Adds a victory to player
     */
    void addVictory();

    /**
     * @return This Player's PlayerID
     */
    PlayerID getPlayerID();

    /**
     * @return This Player's Color
     */
    Color getColor();

    /**
     * @return This Player's Symbol
     */
    Symbol getSymbol();

    /**
     * @return This Player's thread
     */
    //Thread getPlayerThread()
}
