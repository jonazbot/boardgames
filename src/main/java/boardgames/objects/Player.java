package boardgames.objects;

import javafx.scene.paint.Color;
import java.util.Arrays;
import java.util.List;


/**
 * Enum. Expandable to add more unique Players.
 */
public enum Player implements IPlayer{
    PlayerNull(null, null, null),
    Player1(PlayerID.P1, Color.RED, Symbol.X),
    Player2(PlayerID.P2, Color.YELLOW, Symbol.O);


    /**
     * The two players: {@link #Player1}, {@link #Player2}.
     */
    public static final List<IPlayer> PLAYERS = Arrays.asList(Player1, Player2);

    private final Color color;
    private final Symbol symbol;
    private final PlayerID playerID;
    private String playerName;
    private int victories;
    private boolean toggledAI;
    //private Thread thread;


    /**
     * A Game Player
     *
     * @param playerID P1, P2. From PlayerID enum
     * @param color RED, YELLOW. Colors in JavaFX
     * @param symbol X, Y. From Symbol enum
     */

    Player(PlayerID playerID, Color color, Symbol symbol) {

        this.playerID = playerID;
        this.playerName = null;
        this.color = color;
        this.symbol = symbol;
        this.victories = 0;
        toggledAI = false;
        //this.thread = thread;


    }


    /**
     * @param id PlayerID
     * @return Player with same PlayerID
     */
    public static IPlayer getPlayerByID(PlayerID id) {
        if (id != null)
            for(IPlayer player :PLAYERS) {
                    if (id.equals(player.getPlayerID()))
                        return player;
        }
        return null;
    }


    @Override
    public void toggleAI(Boolean state) { toggledAI = state; }

    @Override
    public boolean isAIToggled() { return toggledAI; }

    @Override
    public void setPlayerName(String name) { this.playerName = name; }

    @Override
    public String getPlayerName() { return playerName; }

    @Override
    public String getVictories() { return Integer.toString(victories); }

    @Override
    public void addVictory() { this.victories++; }

    @Override
    public PlayerID getPlayerID() { return playerID; }

    @Override
    public Color getColor() { return color; }

    @Override
    public Symbol getSymbol() { return symbol; }

    //Thread getPlayerThread() { return thread; }

}

