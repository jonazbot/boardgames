package boardgames.game;

import boardgames.grid.Direction;
import boardgames.grid.IGrid;
import boardgames.objects.IPlayer;
import boardgames.objects.Player;
import boardgames.objects.PlayerID;

import java.util.List;

public class WinCondition {

    private final int connectedToWin;
    private final IGame game;
    IGrid gameGrid;
    private IPlayer winner;
    private boolean hasWinner;

    private final boolean debugMode;   // Debug Mode. Set Game::debugMode = true;


    /**
     * Sets up the win conditions rules of the boardgames.game
     *
     * @param game
     */
    public WinCondition(IGame game) {
        this.game = game;
        gameGrid = game.getGrid();
        winner = null;
        hasWinner = false;
        this.connectedToWin = game.getConnectedToWin();
        debugMode = game.getDebugMode();
    }


    /**
     * Checks win conditions
     *
     * @param x Grid x-coordinate
     * @param y Grid y-coordinate
     * @return true if the move caused enough tiles in a row to win
     */
    public boolean checkWin(int x, int y) {
        if(!hasWinner)
            if(checkLine(x,y)) {
                hasWinner = true;
                winner = Player.getPlayerByID((PlayerID) gameGrid.get(x, y));
                if (debugMode) System.out.println(winner + " has Won the Game!");
                return true;
            }
        return false;
    }


    /**
     * Checks each line of opposing directions and counts the consecutively connected tiles
     *
     * @param x Grid x-coordinate
     * @param y Grid y-coordinate
     * @return True if a line has enough connectedToWin in a row
     */
    boolean checkLine(int x, int y) {

        if (gameGrid.get(x, y) != null) {       // Hail Murphy!
            PlayerID playerID = (PlayerID) gameGrid.get(x, y);

            // Loop though paired opposing directions
            for (List<Direction> oppositeDirections : Direction.OPPOSING_PAIRS_LIST) {
                int connected = 1;

                // Loop though each opposing direction...
                for (Direction direction : oppositeDirections) {
                    int dx = x;
                    int dy = y;

                    // ... for enough tiles to win...
                    for(int i=0; i<connectedToWin; i++) {
                        dx += direction.getX();
                        dy += direction.getY();

                        // ... but only check inside the board
                        if (dx >= 0 && dx < gameGrid.getWidth() && dy >= 0 && dy < gameGrid.getHeight()) {
                            PlayerID nextTile = (PlayerID) gameGrid.get(dx, dy);
                            if (nextTile != null && nextTile.equals(playerID))      // Hail Murphy!
                                connected++;
                            else break;
                        }
                    }
                }
                if (connected >= connectedToWin) { return true; }
            }
        }
        return false;
    }

    /**
     * Checks if there has been a winner
     *
     * @return True if it has found a winner, false otherwise.
     */
    public boolean hasWinner() { return hasWinner; }

    /**
     * @return IPlayer winner
     */
    public IPlayer getWinner() { return winner; }


}
