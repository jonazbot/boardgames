package boardgames.game;

import boardgames.AI.GameAIChild;
import boardgames.GUI.GUIController;
import boardgames.grid.IGrid;
import boardgames.objects.IPlayer;
import boardgames.objects.Player;
import boardgames.objects.PlayerID;
import javafx.application.Platform;

import java.util.concurrent.Semaphore;

abstract class Game implements IGame {

    protected int width, height;
    protected int connectedToWin;
    protected int turn;
    protected boolean physics;
    protected Thread gameThread;
    protected IGrid<PlayerID> board;
    protected WinCondition rules;
    protected IPlayer currentPlayer;
    protected volatile boolean stopGame;
    protected static Semaphore mutex;
    protected static Semaphore mutexInner;
    protected GUIController guiControl;
    protected GameAIChild gameAIChild;

    private final boolean debugMode = false; // Debugging tool


    @Override
    public void round() {

        synchronized (this) {
            try { wait(10); }
            catch (InterruptedException e) { e.printStackTrace(); }
        }

        for (IPlayer player : Player.PLAYERS) {
            currentPlayer = player;
            if (debugMode) { System.out.println("Next: " + currentPlayer); }

            if (currentPlayer.isAIToggled()) { playAI(); }
            else {
                synchronized (this) {
                    try { wait(); }
                    catch (InterruptedException e) { e.printStackTrace(); }
                }
            }

            if (stopGame) { break; }
        }

        if (debugMode) { System.out.println("Round Over"); }

    }

    @Override
    public void playGame() {
        while(!stopGame) {
            if (getDebugMode()) { System.out.println("Round Start"); }
            round();
            if (getDebugMode()) { System.out.println("Round End"); }
        }

    }


    @Override
    public void checkValidMove(int x, int y) {

        if (!stopGame && getGrid().get(x, y) == null) {
            if (physics) {
                y = physics(x);
                if (y == -1) { return; } // Error. Should never happen.
            }
            if (debugMode) { System.out.println("Performing move ("+x+" ,"+y+")"); }
            attemptTurn(x, y);
            synchronized(this) { notify(); }    // Notify Game Thread Player made a move.
        }
    }


    @Override
    public void attemptTurn(int x, int y)  {

        try {
            mutex.acquire();
            try {
                mutexInner.acquire(); if (debugMode) { System.out.println("Mutex Acquired"); }
                // TODO: Check if thread owner is currentPlayer
                performTurn(x, y);
                mutexInner.release(); if (debugMode) { System.out.println("Inner Mutex Released"); }
            } catch (Exception e) { if (debugMode) { System.out.println("Inner Mutex Error!"); }}
            mutex.release(); if (debugMode) { System.out.println("Mutex Released"); }
        } catch (Exception e) { if (debugMode) { System.out.println("Mutex Lock Error!"); }}
    }


    @Override
    public void performTurn(int x, int y) {

        board.set(x, y, currentPlayer.getPlayerID());
        setGameGUI(x, y, currentPlayer);

        if (checkWin(x, y)) { signalWinner(); }
        else if (this.getGrid().fullBoard()) { stopGame(); }
        else { nextTurn(); this.signalTurnGUI(); }

        if (debugMode) { printBoard(); }    //Prints board to console
    }


    @Override
    public int physics(int x) {

        for (int y = height-1; y >= 0; y--)
            if (board.get(x, y) == null)
                return y;
        return -1;  // Error
    }

    @Override
    public boolean checkWin(int x, int y){

        IPlayer currentPlayer = this.getCurrentPlayer();
        if (rules.checkWin(x, y)) {
            this.signalScoreLabels();
            currentPlayer.addVictory();
            stopGame();
            return true;
        }
        return false;
    }


    @Override
    public void printBoard() {

        for (int y = 0; y < board.getHeight(); y++){
            for(int x = 0; x < board.getWidth(); x++) {
                if (board.get(x, y) != null) { System.out.print(" "+board.get(x, y) + "  "); }
                else { System.out.print(board.get(x, y) + " "); }
            }
            System.out.println();
        }
    }


    @Override
    public void nextTurn() { turn++; }

    @Override
    public void playAI() { gameAIChild.makeMove(this); }

    @Override
    public int getWidth() { return this.width; }

    @Override
    public int getHeight() { return this.height; }

    @Override
    public IPlayer getCurrentPlayer() { return currentPlayer; }

    @Override
    public int getConnectedToWin() { return connectedToWin; }

    @Override
    public IGrid<PlayerID> getGrid() { return board; }

    @Override
    public WinCondition getRules() { return rules; }

    @Override
    public void stopGame() { stopGame = true; }

    @Override
    public void signalWinner() { Platform.runLater(() -> guiControl.showWinner()); }

    @Override
    public void signalScoreLabels() { Platform.runLater(() -> guiControl.setScoreLabels()); }

    @Override
    public void signalTurnGUI() { Platform.runLater(() -> guiControl.setTurnLabels(String.valueOf(turn), currentPlayer)); }

    @Override
    public boolean gameStopped() { return stopGame; }

    @Override
    public void setGameThread(Thread thread) { gameThread = thread; }

    @Override
    public boolean getDebugMode() { return debugMode; }

}
