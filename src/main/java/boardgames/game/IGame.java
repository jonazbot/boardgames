package boardgames.game;

import boardgames.grid.IGrid;
import boardgames.objects.IPlayer;

public interface IGame extends Runnable {


    /**
     * Heart of the Game Engine. Rotates between Player turns and performs moves AI moves.
     * Also stops the Game Thread on signal @stopGame
     */
    void round();


    /**
     * Starts the game thread cycle
     */
    void playGame();

    /**
     * Checks if player mouse event is a valid move
     *
     * @param x Grid X-coordinate
     * @param y Grid Y-coordinate
     */
    void checkValidMove(int x, int y);


    /**
     * Attempts to get a mutex lock to access the mutex lock that protects the @ performTurn() method.
     *
     * @param x Grid X-coordinate
     * @param y Grid Y-coordinate
     */
    void attemptTurn(int x, int y);

    /**
     * CRITICAL SECTION!
     *
     * Performs turn and signals boardgames.GUI
     *
     * @param x Grid X-coordinate
     * @param y Grid Y-coordinate
     */
    void performTurn(int x, int y);


    /**
     * @return Player whos turn it is
     */
    IPlayer getCurrentPlayer();

    /**
     *
     * @return int, number in a row to win
     */
    int getConnectedToWin();

    /**
     * @return boardgames.game Grid
     */
    IGrid getGrid();

    /**
     * Adds to turn-counter
     */
    void nextTurn();

    /**
     * Simulates tile dropping to lowest available slot
     *
     * @param x Grid X-coordinate
     * @return int Grid Y-coordinate value of where a tile will land
     */
    int physics(int x);

    /**
     * Makes GameAIChild make random move
     */
    void playAI();

    /**
     * Checks if a tile being placed on the board causes someone to win
     *
     * @param x Grid X-coordinate
     * @param y Grid Y-coordinate
     * @return True if the tile played caused WinCondition, false otherwise.
     */
    boolean checkWin(int x, int y);


    /**
     * Enable debugMode to print board to console
     */
    void printBoard();


    /**
     * @return width of boardgames.game boardgames.grid
     */
    int getWidth();


    /**
     * @return height of boardgames.game boardgames.grid
     */
    int getHeight();

    /**
     * @return WinCondition held by Game
     */
    WinCondition getRules();

    /**
     * Signals Thread to stop after this turn is completed
     */
    void stopGame();

    void signalWinner();

    void signalScoreLabels();

    /**
     * Setter method for setting Turn Labels in GUIController
     */
    void signalTurnGUI();

    /**
     * Checks if boardgames.game should be stopped
     * @return True if gameStop = true, false if not.
     */
    boolean gameStopped();

    /**
     * Sets the thead of the boardgames.game
     *
     * @param thread Game Thread
     */
    void setGameThread(Thread thread);

    /**
     * @param x IGrid x-coordinate
     * @param y IGrid y-coordinate
     * @param player Player who made a move
     */
    void setGameGUI(int x, int y, IPlayer player);

    boolean getDebugMode();
}
