package boardgames.game;

import boardgames.AI.GameAIChild;
import boardgames.GUI.GUIController;
import boardgames.grid.Grid;
import boardgames.objects.IPlayer;
import boardgames.objects.Player;
import boardgames.objects.PlayerID;
import javafx.application.Platform;

import java.util.concurrent.Semaphore;

public class TicTacToeGame extends Game implements Runnable {

    public TicTacToeGame(GUIController guiController) {

        guiControl = guiController;
        width = 3;
        height = 3;
        connectedToWin = 3;
        turn = 0;
        physics = false;
        board = new Grid<PlayerID>(width, height, Player.PlayerNull.getPlayerID());
        rules = new WinCondition(this);
        currentPlayer = Player.Player1;
        gameAIChild = new GameAIChild();
        stopGame = false;
        mutex = new Semaphore(1);
        mutexInner = new Semaphore(1);
    }


    /**
     * Game Thread behavior. Performs a round until it is stopped.
     */
    @Override
    public void run() {
        playGame();
    }

    @Override
    public void setGameGUI(int x, int y, IPlayer player) {
        if (getDebugMode()) { System.out.println("Setting boardgames.GUI X: "+x+" Y: "+y+" Player: "+player); }
        Platform.runLater(() -> { guiControl.getTicTacToeControl().setTile(x, y, player); this.signalTurnGUI();});
        if (getDebugMode()) { System.out.println("Done setting boardgames.GUI X: "+x+" Y: "+y+" Player: "+player); }
    }


}