package boardgames.GUI;

import boardgames.game.ConnectFourGame;
import boardgames.grid.Grid;
import boardgames.objects.IPlayer;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Circle;

import java.util.*;

public class ConnectFourController implements IGameController{


    private final ConnectFourGame game;
    private final Thread connectFour;

    private List<Circle> circleList;

    @FXML
    Circle  circle00, circle10, circle20, circle30, circle40, circle50, circle60,
            circle01, circle11, circle21, circle31, circle41, circle51, circle61,
            circle02, circle12, circle22, circle32, circle42, circle52, circle62,
            circle03, circle13, circle23, circle33, circle43, circle53, circle63,
            circle04, circle14, circle24, circle34, circle44, circle54, circle64,
            circle05, circle15, circle25, circle35, circle45, circle55, circle65;

    @FXML
    BorderPane gamePane;

    public ConnectFourController(GUIController guiController) {

        game = new ConnectFourGame(guiController);
        connectFour = new Thread(game);
        game.setGameThread(connectFour);

    }

    /**
     * Initializes fxml variables, and starts the Game tread.
     */
    public void initialize() {

        circleList = Arrays.asList(
                circle00, circle10, circle20, circle30, circle40, circle50, circle60,
                circle01, circle11, circle21, circle31, circle41, circle51, circle61,
                circle02, circle12, circle22, circle32, circle42, circle52, circle62,
                circle03, circle13, circle23, circle33, circle43, circle53, circle63,
                circle04, circle14, circle24, circle34, circle44, circle54, circle64,
                circle05, circle15, circle25, circle35, circle45, circle55, circle65);

        connectFour.start();
    }


    /**
     * Event Listener for Circles
     * Checks if Circle clicked on is a valid move
     *
     * @param mouseEvent MouseEvent
     */
    @Override
    public void mouseClickTile(MouseEvent mouseEvent) {

        String sourceString =((Circle) mouseEvent.getSource()).getId();
        int x = Integer.parseInt(sourceString.substring(sourceString.length()-2, sourceString.length()-1));
        int y = Integer.parseInt(sourceString.substring(sourceString.length()-1));
        if (game.getDebugMode()) { System.out.println("Mouse Click X: "+x+", Y: "+y); }
        game.checkValidMove(x, y);
    }

    @Override
    public void setTile(int x, int y, IPlayer player) {
        if (game.getDebugMode()) { System.out.println("Setting Circle X: "+x+", Y: "+y+" Player: "+player); }
        Circle playerTile = circleList.get(Grid.GridToListIndex(x, y, game.getWidth()));
        playerTile.setFill(player.getColor());
        playerTile.setMouseTransparent(true);

        if (game.getDebugMode()) { System.out.println("Done setting Circle X: "+x+", Y: "+y+" Player: "+player); }
    }


}
