package boardgames.GUI;

import boardgames.objects.IPlayer;
import boardgames.objects.Player;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import java.io.IOException;

public class GUIController implements IController {

    private static final String ticTacToeText = " Tic Tac Toe ";
    private static final String connectFourText = " Connect Four ";
    private String player1Name, player2Name;
    private boolean p1AIToggled, p2AIToggled;

    private final GUIController guiControl;
    private ConnectFourController connectFourControl;
    private TicTacToeController ticTacToeControl;

    public GUIController() {

        guiControl = this;

    }

    @FXML
    CheckMenuItem toggleAI1, toggleAI2;

    @FXML
    BorderPane gamePane;

    @FXML
    Label gameLabel, nameP1, nameP2, turnNumber, scoreP1, scoreP2, turnPlayer, turnPlayerAvatar, winLabel;


    public void initialize() {

        player1Name = "Player1";
        player2Name = "Player2";
        p1AIToggled = false;
        p2AIToggled = false;
    }

    //File MenuItems
    /**
     * Quits. Controlled by quitMenuItem
     */
    public void quit() {
        System.exit(0);
    } // Needs to also stop the threads

    /**
     * Starts a new boardgames.game of what you were playing.
     */
    public void newGame() {
        if (gameLabel.getText().equals(ticTacToeText)) { playTicTacToe(); }
        else if (gameLabel.getText().equals(connectFourText)) { playConnectFour(); }
        }


    //Games MenuItems
    /**
     * Sets up the TicTacToeController and loads TicTacToe.fxml boardgames.GUI and puts it in gamePane
     */
    public void playTicTacToe() {

        ticTacToeControl = new TicTacToeController(guiControl);
        FXMLLoader ticTacToeLoader = new FXMLLoader(getClass().getResource("/TicTacToe.fxml"));
        ticTacToeLoader.setController(ticTacToeControl);

        try { ticTacToeLoader.load(); }
        catch (IOException e) { e.printStackTrace(); }

        BorderPane ticTacToePane = ticTacToeLoader.getRoot();
        ticTacToePane.getStylesheets().add(getClass().getResource("/TicTacToe.css").toExternalForm());
        gamePane.setCenter(ticTacToePane);
        gameLabel.setText(ticTacToeText);
        setTurnLabels("0", Player.Player1);
        winLabel.setVisible(false);
    }

    /**
     * Sets up the ConnectFourController and loads ConnectFour.fxml boardgames.GUI and puts it in gamePane
     */
    public void playConnectFour() {

        connectFourControl = new ConnectFourController(guiControl);
        FXMLLoader connectFourLoader = new FXMLLoader(getClass().getResource("/ConnectFour.fxml"));
        connectFourLoader.setController(connectFourControl);

        try { connectFourLoader.load(); }
        catch (IOException e) { e.printStackTrace(); }

        BorderPane fourInARowPane = connectFourLoader.getRoot();
        fourInARowPane.getStylesheets().add(getClass().getResource("/ConnectFour.css").toExternalForm());
        fourInARowPane.setMinSize(490, 420);
        gamePane.setCenter(fourInARowPane);
        gameLabel.setText(connectFourText);
        setTurnLabels("0", Player.Player1);
        winLabel.setVisible(false);
    }

    //Player Menu - TEMPORARILY REMOVED
    /**
     * Set Player1 name
     */
    public void setPlayer1Name() {
        Player.Player1.setPlayerName(player1Name);
        nameP1.setText(player1Name);
    }

    /**
     * Set Player2 name
     */
    public void setPlayer2Name() {
        Player.Player2.setPlayerName(player2Name);
        nameP2.setText(player2Name);
    }


    // AI Menu
    /**
     * Toggles Player 1 AI on and off. Is controlled by the MenuBarItem toggleAI1
     */
    public void toggleAIRandomP1() {
        p1AIToggled = !p1AIToggled;
        Player.Player1.toggleAI(p1AIToggled);
        //System.out.println("P1 AI "+p1AIToggled);
    }

    /**
     * Toggles Player 2 AI on and off. Is controlled by the MenuBarItem toggleAI2
     */
    public void toggleAIRandomP2() {
        p2AIToggled = !p2AIToggled;
        Player.Player2.toggleAI(p2AIToggled);
        //System.out.println("P2 AI "+p2AIToggled);
    }

    /**
     * Checks if p1AIToggled has been enabled by toggleAI1
     *
     * @return True if toggled, false otherwise
     */
    public boolean isP1AIToggled() { return p1AIToggled; }

    /**
     * Checks if p1AIToggled has been enabled by toggleAI1
     *
     * @return True if toggled, false otherwise
     */
    public boolean isP2AIToggled() { return p2AIToggled; }


    /**
     * Sets Score Labels in boardgames.GUIController
     */
    public void setScoreLabels() {

        scoreP1.setText(Player.Player1.getVictories());
        scoreP2.setText(Player.Player2.getVictories());
    }

    /**
     * Sets Turn Labels in boardgames.GUI
     *
     * @param turn String of turn number
     * @param player IPlayer who's turn it is
     */
    public void setTurnLabels(String turn, IPlayer player) {

        turnNumber.setText(turn);
        if (gameLabel.getText().equals(ticTacToeText)) {
            turnPlayerAvatar.setText(player.getSymbol().toString());
            turnPlayerAvatar.setTextFill(Color.BLACK);
        }
        else if (gameLabel.getText().equals(connectFourText)) {
            turnPlayerAvatar.setText("●");
            turnPlayerAvatar.setTextFill(player.getColor());
        }
        if (player.getPlayerName() != null)
            turnPlayer.setText(player.getPlayerName());
        else
            turnPlayer.setText(player.toString());
    }

    /**
     * Displays Winner Label in boardgames.GUI
     */
    public void showWinner() {
        winLabel.setVisible(true);
    }

    /**
     * @return this GUIController
     */
    public GUIController getGuiControl() { return guiControl; }

    /**
     * @return TicTacToeController for the TicTacToeGame
     */
    public TicTacToeController getTicTacToeControl() { return ticTacToeControl; }

    /**
     * @return ConnectFourController for the ConnectFourGame
     */
    public ConnectFourController getConnectFourControl() { return connectFourControl; }
}