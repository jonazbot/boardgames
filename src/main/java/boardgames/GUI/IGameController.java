package boardgames.GUI;

import boardgames.objects.IPlayer;
import javafx.scene.input.MouseEvent;

public interface IGameController extends IController {

    /**
     * Event Listener
     * Checks if cell clicked on is a valid move
     *
     * @param mouseEvent MouseEvent
     */
    void mouseClickTile(MouseEvent mouseEvent);

    /**
     * Sets the cell corresponding to gameGrid in the boardgames.GUI
     *
     * @param x Grid x-coordinate
     * @param y Grid y-coordinate
     * @param player IPlayer playing the tile
     */
    void setTile(int x, int y, IPlayer player);
}
