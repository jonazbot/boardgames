package boardgames.GUI;

import boardgames.game.TicTacToeGame;
import boardgames.grid.Grid;
import boardgames.objects.IPlayer;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class TicTacToeController implements IGameController{

    private final TicTacToeGame game;
    private final Thread ticTacToe;

    private final List<Text> textList = new ArrayList<>();
    private final List<Pane> tileList = new ArrayList<>();

    @FXML
    Pane    tile00, tile10, tile20,
            tile01, tile11, tile21,
            tile02, tile12, tile22;

    @FXML
    Text    text00, text10, text20,
            text01, text11, text21,
            text02, text12, text22;


    public TicTacToeController(GUIController guiController) {

        game = new TicTacToeGame(guiController);
        ticTacToe = new Thread(game);
        game.setGameThread(ticTacToe);
    }

    /**
     * Sets up lists of tiles and text fields and starts the Game thread.
     */
    public void initialize() {

        tileList.add(tile00);
        tileList.add(tile10);
        tileList.add(tile20);
        tileList.add(tile01);
        tileList.add(tile11);
        tileList.add(tile21);
        tileList.add(tile02);
        tileList.add(tile12);
        tileList.add(tile22);

        textList.add(text00);
        textList.add(text10);
        textList.add(text20);
        textList.add(text01);
        textList.add(text11);
        textList.add(text21);
        textList.add(text02);
        textList.add(text12);
        textList.add(text22);

        ticTacToe.start();
    }


    /**
     * Event Listener for tiles (Panes)
     * Checks if tile clicked on is a valid move.
     *
     * @param mouseEvent
     */
    @Override
    public void mouseClickTile(MouseEvent mouseEvent) {

        String sourceString = ((Pane) mouseEvent.getSource()).getId();
        int x = Integer.parseInt(sourceString.substring(sourceString.length() - 2, sourceString.length() - 1));
        int y = Integer.parseInt(sourceString.substring(sourceString.length() - 1));
        if (game.getDebugMode()) { System.out.println("Mouse Click X: "+x+", Y: "+y); }
        game.checkValidMove(x, y);
    }

    @Override
    public void setTile(int x, int y, IPlayer player) {

        if (game.getDebugMode()) { System.out.println("Setting Tile X: "+x+", Y: "+y+" Player: "+player); }

        // Set Tile to Symbol and make it unresponsive
        int index = Grid.GridToListIndex(x, y, game.getGrid().getWidth());
        textList.get(index).setText(player.getSymbol().toString());
        tileList.get(index).setMouseTransparent(true);

        if (game.getDebugMode()) { System.out.println("Done setting Tile X: "+x+", Y: "+y+" Player: "+player); }
    }
}
