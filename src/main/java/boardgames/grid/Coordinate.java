package boardgames.grid;

public class Coordinate {
    private final int x;
    private final int y;

    public Coordinate (int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return X-coordinate
     */
    public int getX() { return this.x;}

    /**
     * @return Y-coordinate
     */
    public int getY() { return this.y;}

}
