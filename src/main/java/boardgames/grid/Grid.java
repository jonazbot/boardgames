package boardgames.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * A Grid contains a set of elements 
 *
 */
public class Grid<T> implements IGrid<T>, Iterable<T>{
	private final List<T> cells;
	private final int height;
	private final int width;

	/**
	 * 
	 * Construct a boardgames.grid with the given dimensions.
	 * 
	 * @param width
	 * @param height
	 * @param initElement
	 *            What the cells should initially hold (possibly null)
	 */
	public Grid(int width, int height, T initElement) {
		if(width <= 0 || height <= 0)
			throw new IllegalArgumentException();

		this.height = height;
		this.width = width;
		cells = new ArrayList<>(height * width);
		for (int i = 0; i < height * width; ++i) {
			cells.add(initElement);
		}
	}

	private int coordinateToIndex(int x, int y) { return x + y*width; }
	
	@Override
	public int getHeight() {
		return height;
	}


	@Override
	public int getWidth() {
		return width;
	}


	@Override
	public void set(int x, int y, T elem) {
		if(x < 0 || x >= width)
			throw new IndexOutOfBoundsException();
		if(y < 0 || y >= height)
			throw new IndexOutOfBoundsException();

		cells.set(coordinateToIndex(x, y), elem);
	}

	@Override
	public T get(int x, int y) {
		if(x < 0 || x >= width)
			throw new IndexOutOfBoundsException();
		if(y < 0 || y >= height)
			throw new IndexOutOfBoundsException();

		return cells.get(coordinateToIndex(x, y)); 
	}

	@Override
	public IGrid<T> copy() {
		Grid<T> newGrid = new Grid<>(getWidth(), getHeight(), null);

		for (int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
				newGrid.set(x,  y,  get(x, y));
		return newGrid;
	}

	@Override
	public Iterator<T> iterator() {
		return cells.iterator();
	}

	@Override
	public boolean fullBoard() {
		for (T t : this) {
			if (t == null)
				return false;
		}
		return true;
	}

	@Override
	public List<Coordinate> getAvailable() {
		List<Coordinate> list = new ArrayList<>();
		for (int y=0; y<this.getHeight();y++ )
			for (int x=0; x<this.getWidth();x++ )
				if (get(x,y) == null)
					list.add(new Coordinate(x, y));
		return list;
	}


	/**
	 * Converts Grid coordinates into list index based on width
	 *
	 * @param x Grid x-coordinate
	 * @param y Grid y-coordinate
	 * @param width Width of the Grid
	 * @return
	 */
	public static int GridToListIndex(int x, int y, int width) { return x + y*width; }

}
