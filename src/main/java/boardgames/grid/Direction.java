package boardgames.grid;

import java.util.Arrays;
import java.util.List;

/**
 * Vertical, Horizontal, and Diagonal directions
 */
public enum Direction {

    NORTH(0, -1), SOUTH(0, 1), EAST(1, 0), WEST(-1, 0), SOUTHWEST(-1, 1), NORTHEAST(1, -1), SOUTHEAST(1, 1), NORTHWEST(-1, -1);

    private final int directionX, directionY;

    Direction(int x, int y) {
        directionX = x;
        directionY = y;
    }

    /**
     * List of Lists containing opposing pairs of Directions
     */
    public static final List<List<Direction>> OPPOSING_PAIRS_LIST = Arrays.asList(Arrays.asList(NORTH, SOUTH), Arrays.asList(EAST, WEST), Arrays.asList(SOUTHWEST, NORTHEAST), Arrays.asList(SOUTHEAST, NORTHWEST));
    /**
     * List of all the directions
     */
    public static final List<Direction> DIRECTIONS = Arrays.asList(NORTH, SOUTH, EAST, WEST, SOUTHWEST, NORTHEAST, SOUTHEAST, NORTHEAST);


    /**
     * @return dX, directional X
     */
    public int getX() { return directionX; }

    /**
     * @return dY, directional Y
     */
    public int getY() { return directionY; }
}
