package boardgames.grid;

import java.util.List;

/**
 * IGrid is a generic boardgames.grid
 * 
 * @author Anna Eilertsen - anna.eilertsen@uib.no 
 *
 */
public interface IGrid<T> {


	/**
	 * @return The height of the boardgames.grid.
	 */
	int getHeight();

	/**
	 * @return The width of the boardgames.grid.
	 */
	int getWidth();

	/**
	 * 
	 * Set the contents of the cell in the given x,y location. 
	 * 
	 * y must be greater than or equal to 0 and less than getHeight().
	 * x must be greater than or equal to 0 and less than getWidth().
	 * 
	 * @param x The column of the cell to change the contents of.
	 * @param y The row of the cell to change the contents of.
	 * @param element The contents the cell is to have.
	 */
	void set(int x, int y, T element);

	/**
	 * 
	 * Get the contents of the cell in the given x,y location. 
	 * 
	 * y must be greater than or equal to 0 and less than getHeight().
	 * x must be greater than or equal to 0 and less than getWidth().
	 * 
	 * @param x The column of the cell to get the contents of.
	 * @param y The row of the cell to get contents of.
	 */
	T get(int x, int y);

	/**
	 * Make a copy
	 *
	 * @return A shallow copy of the boardgames.grid, with the same elements
	 */
	IGrid<T> copy();

	/**
	 * Checks if boardgames.game board is full of tiles
	 *
	 * @return True if board is full, false if it isn't
	 */
	boolean fullBoard();


	/**
	 * @return List of Coordinates representing available cells
	 */
	List<Coordinate> getAvailable();
}